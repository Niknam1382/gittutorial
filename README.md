# __Hello__
This is a repo to practice your git. Try and add your name to the list!
we really enjoy of your teaching. thank you so much.

`- [Yourname](your gitlab profile url)`

feel free to use these emojis: https://gist.github.com/rxaviers/7360908 :relaxed:

Learning Source: https://faradars.org/courses/fvgit9609-managed-distributed-edition-using-git :v:

# People who know git (more or less)

- jadi
- hashemsh
- Hasan
- [jadijadi](https://gitlab.com/jadijadi)
- [AlirezaNjR](https://gitlab.com/AlirezaNjR/)
- AliReza Ghenaatiyan
- seighalani
- Majid
- saeed payvandi
- soroush
- amir.valikhani
- Mohamad Mohamadi
- Mamad Bigdeli
- Hossein Sharifi 
- soroush
- wahab safi
- razie
- Hessam
- jadi
- shamsnaamir
- niloufar
- niloufar
- morteza noroozi (:
- orbsim
- rezaduty
- kahk313
- hashemsh
- ayman
- toma
- Mojtaba
- jadi
- Seyed
- [__Jadi__ :heart:](https://jadi.net)
- [__Fakhamatia__ :metal:](https://gitlab.com/fakhamatia)
- shamsnaamir
- saeed
- meisam
- M_fallahi
- abolfazl :)
- Salimeh
- ehsanmody
- Mehdi Savari
- samira
- Mehdi Savari
- Salimeh
- ehsanmody
